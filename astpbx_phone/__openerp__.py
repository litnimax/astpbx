# -*- coding: utf-8 -*- 
 
{
    'name': 'AstPBX Phone',
    'version': '0.1',
    'category': 'Asterisk',
    #'sequence': 3, #? WTF
    'author': 'Max Litnitsky',
    'summary': 'Asterisk Phone Module',
    'description': """
Manage Asterisk PBX server - Web Phone 
===============================================
Under construction
    """,
    'depends': ['im_chat'],
    'data': [
    'views/document.xml',
        'astpbx_phone_view.xml', 
    ],
    'js': ['static/src/js/astpbx_phone.js'],
    'css': ['static/src/css/phone.css'],
    'qweb': ['static/src/xml/astpbx_phone.xml'],
    'installable': True,
    'application': True,
    'auto_install': True,
}


    
