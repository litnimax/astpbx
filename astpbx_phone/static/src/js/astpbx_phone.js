
openerp.astpbx_phone = function(instance) {    
    instance.astpbx_phone = {};
    
    //var module = instance.astpbx_phone;
    var module = instance.im_chat;
        
    /*var _super_ = module.InstantMessaging.prototype.start;

    module.ConversationManager.prototype.start = function(){
     console.log('Hello!');
     _super_.call(this);
    };
    */  
        
    var QWeb = instance.web.qweb;
        
    module.InstantMessaging.include({
        start: function () {
            this._super();
            console.log('Started from chat');
            this.$el.append(QWeb.render("WebPhoneTemplate"));
        },
    });
        
        
    instance.astpbx_phone.HomePage = instance.web.Widget.extend({
        start: function() {
            this.$el.append(QWeb.render("WebPhoneTemplate"));
        },
    });
    
    
    
    instance.web.client_actions.add('pbx_phone.homepage', 'instance.astpbx_phone.HomePage');
};