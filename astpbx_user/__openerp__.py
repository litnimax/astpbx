# -*- coding: utf-8 -*- 
 
{
    'name': 'AstPBX User Portal',
    'version': '0.1',
    'category': 'Asterisk',
    #'sequence': 3, #? WTF
    'author': 'Max Litnitsky',
    'summary': 'Asterisk PBX Basic Module',
    'description': """
finally: pbx is for user or admin? That's the question :-)
===============================
Under construction
    """,
    'depends': ['web', 'mail'],
    'data': [
	'views/astpbx_user_view.xml',
	'views/cdr_view.xml',
	'views/phonebook_view.xml',
	'security/ir.model.access.csv',
	'security/astpbx_user_security.xml',
    ],
    'js': ['static/src/js/*.js'],
    'css': [],
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
}


    
