from openerp import fields, models, api


TIMEOUT_CHOICES = (
    ('0', 'Immediately'),
    ('3', '1 ring'),
    ('6', '2 rings'),
)


class call_rule(models.Model):
    _name = 'astpbx.user.callrule'
    _rec_name = 'number'
    _order = 'sequence'
    
    user_id = fields.Many2one('res.users', required=True)
    sequence = fields.Integer(required=True)
    number = fields.Char(string='Phone number', required=True)
    timeout = fields.Selection(TIMEOUT_CHOICES, string='Number of rings',
            help='Number of times to ring this number before going to the next one')
    aknowledge = fields.Boolean(string='Call aknowledge', 
            help='Let the System tell callerid number before answering the call')

"""

    _columns = {
        'cf_uncond_is_enabled': fields.boolean(string='Unconditional call forwarding enabled'),
        'cf_uncond_option_ids': fields.many2many('astpbx.user.cf.option', string='')
        'cf_busy_is_enabled'
        'cf_busy_number'
        'cf_noanswer_is_enabled'
        'cf_noanswer_number'
        'dnd_mode'
        'black_mode_is_enabled'
        'white_mode_is_enabled'
        
    

        
class cf_option(orm.Model):
    _name = 'astpbx.user.cf.option'
    
    
            
            
class black_phonebook(orm.Model):
    
class white_phonebook(orm.Model):

"""
