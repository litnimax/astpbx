from openerp import tools
from openerp.osv import orm, fields

class cel_report(orm.Model):
    _name = 'astpbx.cel.report'
    _auto = False
    
    _columns = {
        'eventtime': fields.datetime(string='Event time', select=1),
        'cid_name': fields.char(size=80, string='CID name', select=1),
        'cid_num': fields.char(size=80, string='CID number', select=1),
        'cid_dnid': fields.char(size=80, string='CID DNID', select=1),
        'exten': fields.char(size=80, string='Extension', 
            help='The extension in the dialplan', select=1),
    }
    
    def init(self, cr):
        tools.drop_view_if_exists(cr, 'astpbx_cel_report')
        cr.execute("""
            CREATE OR REPLACE VIEW astpbx_cel_report AS (
                SELECT
                    id,
                    eventtime,
                    exten,
                    cid_num,
                    cid_name,
                    cid_dnid
                FROM astpbx_cel
                    WHERE eventtype = 'HANGUP'
            )
        """
        )
        
