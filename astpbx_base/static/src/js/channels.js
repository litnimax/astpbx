
openerp.astpbx_base = function(instance) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.astpbx_base = {};

    instance.astpbx_base.Channels = instance.web.Widget.extend({
        template: 'astpbx_base.channels',
        
        start: function() {
            $.ajax("http://localhost:8088/ari/channels?api_key=dialer:test", {
                type: "GET",
                dataType: "json",
                contentType: "application/json",
            }).then(function(channels) {
                $('.channels_list').html(
                    $(instance.qweb.render('astpbx_base.channels.list', {'channels': channels})));
                console.log("channels:", channels);
            });            
            
        },
        
    });
    
    instance.web.client_actions.add('astpbx_base.channels', 'instance.astpbx_base.Channels');
}