from openerp.osv import fields, osv

class astpbx_base_config_settings(osv.osv_memory):
    _name = 'astpbx.config.settings'
    _inherit = 'res.config.settings'
    _columns = {
        'asterisk_binary': fields.char('Path to Asterisk binary',
            help='Full path to asterisk binary.\n'
                 '-This will be called asterisk -rx "command"'),
    }

