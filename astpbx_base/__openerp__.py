# -*- coding: utf-8 -*- 
 
{
    'name': 'AstPBX Base',
    'version': '0.1',
    'category': 'Asterisk',
    #'sequence': 3, #? WTF
    'author': 'Max Litnitsky',
    'summary': 'Asterisk PBX Basic Module',
    'description': """
Manage Asterisk PBX server
===============================
Under construction
    """,
    'depends': ['web', 'mail'],
    'data': [
	'security/ir.model.access.csv',
	'security/astpbx_base_security.xml',
	'views/res_users_view.xml',
	'res_config_view.xml',
        'base_data.xml',
        'base_view.xml',
	'views/dialplan_view.xml',
	'views/exten_view.xml',
        'views/sip_peer_view.xml',
        'views/sip_peer_data.xml',
	'views/cel_view.xml',
	'views/cdr_view.xml',
	'views/recording_view.xml',
	'views/channels_view.xml',
	#'report/cel_report_view.xml',
	
    ],
    'js': ['static/src/js/*.js'],
    'css': [],
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
}


    
