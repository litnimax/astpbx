import openerp
from openerp.osv import orm, fields
from openerp.tools.translate import _


class base(orm.Model):
    _name = 'astpbx.base'
    
    _columns = {
        'name': fields.char(string='Name', required=True),
        'description': fields.text(string='Description'),
        'ami_host': fields.char(string="AMI Host"),
        'ami_port': fields.char(string="AMI Port"),
        
    }


class server(orm.Model):
    _name = 'astpbx.server'
    
    _columns = {
        'name': fields.char(string='Name', required=True),
        'description': fields.text(string='Description'),
        'ipaddr': fields.char(string='IP Address', required=True),
    }

        

class ast_config(orm.Model):
    _name = 'astpbx.config'
    
    _columns = {
        'cat_metric': fields.integer(select=True),
        'var_metric': fields.integer(select=True),
        'commented': fields.integer(),
        'filename': fields.char(size=128),
        'category': fields.char(size=128),
        'var_name': fields.char(size=128),
        'var_val': fields.text(),
    
    }
    
    _sql = 'CREATE INDEX commented_filename ON astpbx_config(commented, filename)'
    
    

class conf_template(orm.Model):
    _name = 'astpbx.conf.template'
    _order = 'name'
    
    _columns = {
        'name': fields.char(string='Name', required=True, select=1),
        'section': fields.char(string='Section' ),
        'options': fields.text(string='Content', required=True),
    }



