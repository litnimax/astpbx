from openerp.osv import orm, fields

class res_users(orm.Model):
    _name = 'res.users'
    _inherit = 'res.users'
    
    _columns = {
        'exten': fields.char(string='PBX Extension', select=1),
    }

    _sql_constraints = [('exten_uniq', 'unique(exten)', 'The exten must be unique.')]

