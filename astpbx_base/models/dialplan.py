from openerp import fields, models, api, _
import commands
import re



EXTENSIONS_CONF = '/home/max/asterisk/a12/etc/asterisk/extensions.conf'
ASTERISK_CMD = '/home/max/asterisk/a12/sbin/asterisk -rx "%s"'

RE_DIALPLAN_NAME = re.compile('^[a-zA-Z0-9\-_]+$')


class dialplan(models.Model):
    _name = 'astpbx.dialplan'
    _order = 'name'
    
    name = fields.Char(string='Context name', required=True)
    note = fields.Char()
    condition_ids = fields.Many2many('astpbx.dialplan.condition', 'astpbx_dialplan_condition_rel', string='Conditions')
    exten_ids = fields.Many2many('astpbx.exten', 'astpbx_dialplan_exten', string='Extensions')
    dialplan_fail_action_ids = fields.Many2many('astpbx.dialplan.fail.action',  
        'astpbx_dialplan_fail_action_rel', string='Fail actions')
    dialplan_ids = fields.Many2many('astpbx.dialplan', 'astpbx_dialplan_include', 
            'dialplan1_id', 'dialplan2_id', string='Included contexts')
    is_custom = fields.Boolean()
    custom_dialplan = fields.Text()
    
    
    @api.multi
    def _check_name(self):
        for dialplan in self.browse():
            if not RE_DIALPLAN_NAME.search(dialplan.name):
                return False
        return True
    
    
    _constraints = [
        (_check_name,
            'Dialplan name must follow Asterisk context name restrictions e.g. mus be [a-zA-Z0-9_-] format',
            ['name']
        ),
    ]
    
    @api.multi
    def gen_globals(self):
        res = ''
        t = self.env['astpbx.conf.template'].search_read([('name', '=', 'extensions.globals')])
        res += '[%s]\n' % t[0]['section']
        res += t[0]['options'] + '\n'
        res += ';\n'
        return res

    @api.multi
    def gen_general(self):
        res = ''
        t = self.env['astpbx.conf.template'].search_read([('name', '=', 'extensions.general')])
        res += '[%s]\n' % t[0]['section']
        res += t[0]['options'] + '\n'
        res += ';\n'
        return res
    
    """
    def hint_builder(self, cr, uid, context=None):
        res = []
        dialplans = self.browse(cr, uid, [], context=context)
        print 'dialplans', dialplans
        for dialplan in dialplans:
            print dialplan
            print dir(dialplan)
            for exten in dialplan.exten_ids:
                print exten
        return res
    """

    @api.multi
    def gen_dialplan(self):
        res = ''
        cr, uid, context = self.env.args
        for dialplan in self:
            for exten in dialplan.exten_ids:
                res += exten.gen_dialplan()[0] + '\n'
        return res
        

    @api.model
    def generate_config(self):
        res = ''
        res += self.gen_globals()
        res += self.gen_general()
        res += self.gen_dialplan()
        open(EXTENSIONS_CONF, 'w').write(res)
    
    
    @api.multi
    def write(self, vals):
        ret = super(dialplan,self).write(vals)
        self.generate_config()
        return ret

class dialplan_condition(models.Model):
    _name = 'astpbx.dialplan.condition'

    sequence = fields.Integer()
    description = fields.Char()
    condition_type = fields.Selection('_get_condition_types', required=True)
    pattern_called = fields.Char()
    pattern_calling = fields.Char()
    pattern_peer = fields.Char()
    
    def _get_condition_types(self):
        """Override me in order to add new conditions. Please
        note that the added key length should not be higher than already-existing
        ones. 
        """
        return [
            ('calling', 'Calling number'),
            ('called', 'Called number'),
            ('peer', 'Peer name'),
            # TODO: ('timeframe', 'By timeframe'),
            #('custom', 'Custom search'),
        ]
            

class dailplan_fail_action(models.Model):
    _name = 'astpbx.dialplan.fail.action'
    
    sequence = fields.Integer()
    name = fields.Char()
    dialplan = fields.Text()




