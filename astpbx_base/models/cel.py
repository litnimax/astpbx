from openerp.osv import orm, fields

CEL_TYPES = (
    ('CHAN_START', 'The time a channel was created'),
    ('CHAN_END', 'The time a channel was terminated'),
    ('ANSWER', 'The time a channel was answered (ie, phone taken off-hook)'),
    ('HANGUP', 'The time at which a hangup occurred'),
    ('CONF_ENTER', 'The time a channel was connected into a conference room'),
    ('CONF_EXIT', 'The time a channel was removed from a conference room'),
    ('CONF_START', 'The time the first person enters a conference room'),
    ('CONF_END', 'The time the last person left a conference room'),
    ('APP_START', 'The time a tracked application was started'),
    ('APP_END', 'The time a tracked application ended'),
    ('PARK_START', 'The time a call was parked'),
    ('PARK_END', 'Unpark event'),
    ('BRIDGE_START', 'The time a bridge is started'),
    ('BRIDGE_END', 'The time a bridge is ended'),
    ('BRIDGE_UPDATE', 'This is a replacement channel (Masquerade)'),
    ('3WAY_START', 'When a 3-way conference starts (usually via attended transfer)'),
    ('3WAY_END', 'When one or all exit a 3-way conference'),
    ('BLINDTRANSFER', 'When a blind transfer is initiated'),
    ('ATTENDEDTRANSFER', 'When an attended transfer is initiated'),
    ('TRANSFER', 'Generic transfer initiated; not used yet...?'),
    ('PICKUP', 'This channel picked up the peer channel'),
    ('FORWARD', 'This channel is being forwarded somewhere else'),
    ('HOOKFLASH', 'So far, when a hookflash event occurs on a DAHDI interface'),
    ('LINKEDID_END', 'The last channel with the given linkedid is retired'),
    ('USER_DEFINED', 'Triggered from the dialplan, and has a name given by the user'),
)

class cel(orm.Model):
    _name = 'astpbx.cel'
    
    _columns = {
        'eventtype': fields.char(size=30, string='Event type', 
            help='The name of the event', select=1),
        'eventtime': fields.datetime(string='Event time', select=1),
        'userdeftype': fields.char(size=255, string='User event type', select=1),
        'cid_name': fields.char(size=80, string='CID name', select=1),
        'cid_num': fields.char(size=80, string='CID number', select=1),
        'cid_ani': fields.char(size=80, string='CID ANI', select=1),
        'cid_rdnis': fields.char(size=80, string='CID RDNIS', select=1),
        'cid_dnid': fields.char(size=80, string='CID DNID', select=1),
        'exten': fields.char(size=80, string='Extension', 
            help='The extension in the dialplan', select=1),
        'context': fields.char(size=80, string='Context'),
        'channame': fields.char(size=80, string='Channel', 
            help='The name assigned to the channel in which the event took place'),
        'appname': fields.char(size=80, string='Application'),
        'appdata': fields.char(size=80, string='Application data'),
        'amaflags': fields.integer(string='AMA flags'),
        'accountcode': fields.char(size=20, string='Account', select=1),
        'peeraccount': fields.char(size=20, string='Peer account', select=1),
        'uniqueid': fields.char(size=150, string='Uniqueid', select=1),
        'linkedid': fields.char(size=150, string='Linked ID', select=1),
        'userfield': fields.char(size=255, string='User field', select=1),
        'peer': fields.char(size=80, string='Other channel', select=1),
    }
    
    