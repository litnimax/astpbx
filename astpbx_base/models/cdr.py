from openerp.osv import orm, fields

"""
DIALSTATUS_TYPES = (
    ('CHANUNAVAIL', 'CHANUNAVAIL'),
    ('CONGESTION', 'CONGESTION'),
    ('NOANSWER', 'NOANSWER'),
    ('BUSY', 'BUSY'),
    ('ANSWER', 'ANSWER'),
    ('CANCEL', 'CANCEL'),
    ('DONTCALL', 'DONTCALL'),
    ('TORTURE', 'TORTURE'),
    ('INVALIDARGS', 'INVALIDARGS'),
)
"""

DISPOSITION_TYPES = (
    ('NO ANSWER', 'No answer'),
    ('FAILED', 'Failed'),
    ('BUSY', 'Busy'),
    ('ANSWERED', 'Answered'),
    ('CONGESTION', 'Congestion'),
)


class cdr(orm.Model):
    _name = 'astpbx.cdr'
    _inherit = ['mail.thread']
    
    def __init__(self, pool, cr):
        super(cdr, self).__init__(pool, cr)
        cr.execute("""CREATE OR REPLACE FUNCTION update_cdr_user_id() RETURNS trigger AS $$
    DECLARE _src_row res_users%ROWTYPE;
            _dst_row res_users%ROWTYPE;
    
    BEGIN
        SELECT * INTO _src_row FROM res_users WHERE 
            exten = NEW.src;
        SELECT * INTO _dst_row FROM res_users WHERE 
            exten = NEW.dst;
        
        IF _src_row.id IS NOT NULL THEN
            NEW.src_user_id = _src_row.id;
            NEW.src_user_exten = _src_row.exten;
        END IF;
        
        IF _dst_row.id IS NOT NULL THEN
            NEW.dst_user_id = _dst_row.id;        
            NEW.dst_user_exten = _dst_row.exten;                
        END IF;
        
        RETURN NEW;
        
    END; $$ LANGUAGE 'plpgsql';

    DROP TRIGGER IF EXISTS update_cdr_user_id  on astpbx_cdr;
    CREATE TRIGGER update_cdr_user_id BEFORE INSERT on astpbx_cdr 
        FOR EACH ROW EXECUTE PROCEDURE update_cdr_user_id();
    """
        )
    


    
    def _get_cdr_user_id(self, cr, uid, ids, field_name, arg, context=None):
        # TODO: is it database optimized???
        """
        This functions returns user based on dst / src field
        Get user id. Get user's exten Compare records src / dst with exten.
        Return matching
        """
        res = {}
        exten = self.pool['res.users'].browse(cr, uid, [uid], context=context)['exten']
        for cdr in self.browse(cr, uid, ids, context=context):
            print 'cdr', cdr
            if cdr.dst == exten or cdr.src == exten:
                res[cdr.id] = uid
        return res
        
    def _search_cdr_user_id(self, cr, uid, obj, name, args, context=None):
        # TODO: is it database optimized???
        """
        This functions returns user based on dst / src field
        Get user id. Get user's exten Compare records src / dst with exten.
        Return matching
        """
        
        res = []
        exten = self.pool['res.users'].browse(cr, uid, [uid], context=context)['exten']
        for cdr in self.browse(cr, uid, ids, context=context):
            print 'cdr', cdr
            if cdr.dst == exten or cdr.src == exten:
                res.append(uid)
        return [('user_id', 'in', res)]

        
    def _get_cdr_cel_ids(self, cr, uid, ids, fieldnames, args, context=None):
        result = dict.fromkeys(ids, False)
        for cdr in self.browse(cr, uid, ids, context=context):
            cel_ids = self.pool['astpbx.cel'].search(cr, uid, 
                [('uniqueid','=',cdr.uniqueid)], context=context)
            result[cdr.id] = cel_ids   
        return result
        

    _columns = {
        'calldate': fields.datetime(string='Call date', select=1),
        'clid': fields.char(size=80, string='Clid', select=1),
        'src': fields.char(size=80, string='Src', select=1),
        'dst': fields.char(size=80, string='Dst', select=1),
        'dcontext': fields.char(size=80, string='Dcontext'),
        'channel': fields.char(size=80, string='Channel', select=1),
        'dstchannel': fields.char(size=80, string='Dst channel', select=1),
        'lastapp': fields.char(size=80, string='Last app'),
        'lastdata': fields.char(size=80, string='Last data'),
        'duration': fields.integer(string='Duration', select=1),
        'billsec': fields.integer(string='Billsec', select=1),
        'disposition': fields.char(size=45, string='Disposition', select=1),
        'amaflags': fields.integer(string='AMA flags'),
        'accountcode': fields.char(size=20, string='Account code', select=1),
        'uniqueid': fields.char(size=150, string='Uniqueid', select=1),
        'userfield': fields.char(size=255, string='Userfield'),
        'peeraccount': fields.char(size=20, string='Peer account', select=1),
        'linkedid': fields.char(size=150, string='Linked id'),
        'sequence': fields.integer(string='Sequence'),
        'src_user_id': fields.many2one('res.users',  string='Src user ID'),
        'dst_user_id': fields.many2one('res.users',  string='Dst user ID'),
        'src_user_exten': fields.char(string='Src user exten'),
        'dst_user_exten': fields.char(string='Dst user exten'),
        #'user_id': fields.function(_get_cdr_user_id, type='many2one', obj='res.users', 
        #    fnct_search=_search_cdr_user_id, method=True, string='CDR user'),
        'cel_ids': fields.function(_get_cdr_cel_ids, method=True, readonly=True, 
            type='one2many', relation='astpbx.cel', string='CDR CELs'),
    }


    def astpbx_user_cel_action(self, cr, uid, ids, context=None):
        print context
        res = self.pool.get('ir.actions.act_window').for_xml_id(cr, uid, 'astpbx_base', 'astpbx_user_cel_action', context)
        res.update({
            'domain': "[('uniqueid','=', '%s')]" % self.browse(cr, uid, ids, context=context).uniqueid,
            'view_mode': 'tree', 
        })
        print res
        return res

        """
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'astpbx.cel',
            'res_id': int(ids[0]),
            'context': context,
            'view_id': 'astpbx_cel_form',
            }

        """