from openerp.osv import orm, fields
from openerp import api

# TODO: to settings

REC_DIR = '/tmp/rec/'

class cdr(orm.Model):
    _inherit = 'astpbx.cdr'
    
    _columns = {
        'is_recorded': fields.boolean(string='Is recorded?', select='1',
            help='The call was recorded.'),
    
    }
        

class exten(orm.Model):
    _inherit = 'astpbx.exten'
    
    _columns = {
        'is_recorded': fields.boolean(string='Is recorded?', select='1',
            help='Check this to activate call recording on this exten.'),
    }
    
    
    @api.one
    @api.model
    def meetme_builder(self):
        return self.same('MixMonitor(${UNIQUEID}.wav)\n')[0]


    def __init__(self, pool, cr):
        init_res = super(exten, self).__init__(pool, cr)
        self.exten_builders.append((50, 'astpbx.exten', 'meetme_builder'))
        return init_res


class dialplan(orm.Model):
    _inherit = 'astpbx.dialplan'
    
    _columns = {
        'is_recorded': fields.boolean(string='Is recorded?', select='1',
            help='Check this to activate call recording on this context.'),
    }

