from openerp.osv import orm, fields
from openerp.tools.translate import _


class conf_asterisk(orm.Model):
    _name = 'astpbx.conf.asterisk'

    _columns = {
        'server': fields.many2one('astpbx.server', 'name'),
        'astetcdir': fields.char(string='astetcdir', 
            help=_('Path to Asterisk configuration files, e.g. /etc/asterisk/')),
        'astmoddir': fields.char(string='astmoddir', 
            help=_('Path to Asterisk module files, e.g. /var/lib/asterisk/modules/')),
        'astdbdir': fields.char(string='astdbdir',
            help=_('Path to Asterisk astdb.sqlite3')),
        'astkeydir': fields.char(string='astkeydir',
            help=_('Path to Asterisk keys folder')),
        'astdatadir': fields.char(string='',
            help=_('Path to Asterisk data files ')),
        'astagidir': fields.char(string='',
            help=_('')),
        'astspooldir': fields.char(string='',
            help=_('')),
        'astrundir': fields.char(string='',
            help=_('')),
        'astlogdir': fields.char(string='',
            help=_('')),
        'astsbindir': fields.char(string='',
            help=_('')),
        # Options
        'debug': fields.char(string='debug',
            help=_('')),
        'verbose': fields.char(string='verbose',
            help=_('')),
        'alwaysfork': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='alwaysfork',
            help=_('')),
        'nofork': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='nofork',
            help=_('')),
        'quiet': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='quiet',
            help=_('')),
        'timestamp': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='timestamp',
            help=_('')),
        'execincludes': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='execincludes',
            help=_('Support #exec in config files.')),
        'console': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='console',
            help=_('')),
        'highpriority': fields.selection((('yes', 'yes'), ('no', 'no')), 
            string='highpriority',
            help=_('')),

        }

