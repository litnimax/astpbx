from openerp.osv import orm, fields
from openerp import api
import ari
from requests.exceptions import HTTPError, ConnectionError

#TODO
ARI_URL = 'http://localhost:8088'
ARI_LOGIN = 'dialer'
ARI_PASS = 'test'    


class sip_peer(orm.Model):
    _name = 'astpbx.sip.peer'
    
    """
      
      Network
     
      
      SIP options
      `regseconds` int(11) DEFAULT NULL,
      `dtmfmode` enum('rfc2833','info','shortinfo','inband','auto') DEFAULT NULL,
      `directmedia` enum('yes','no','nonat','update') DEFAULT NULL,
      
      `trustrpid` enum('yes','no') DEFAULT NULL,
      `session-timers` enum('accept','refuse','originate') DEFAULT NULL,
      `session-expires` int(11) DEFAULT NULL,
      `session-minse` int(11) DEFAULT NULL,
      `session-refresher` enum('uac','uas') DEFAULT NULL,
      
      Telephony
        `videosupport` enum('yes','no') DEFAULT NULL,
       `callgroup` varchar(40) DEFAULT NULL,
      `pickupgroup` varchar(40) DEFAULT NULL,
       `progressinband` enum('yes','no','never') DEFAULT NULL,
      `allow` varchar(40) DEFAULT NULL,
      `disallow` varchar(40) DEFAULT NULL,
        
    VM & MOH
        `mailbox` varchar(40) DEFAULT NULL,
        `mohinterpret` varchar(40) DEFAULT NULL,
        `mohsuggest` varchar(40) DEFAULT NULL,
        `allowtransfer` enum('yes','no') DEFAULT NULL,
      
      Other
      `language` varchar(40) DEFAULT NULL,
       `accountcode` varchar(40) DEFAULT NULL,
      `setvar` varchar(40) DEFAULT NULL,
       `amaflags` varchar(40) DEFAULT NULL,
       
       
      
      `defaultuser` varchar(10) DEFAULT NULL,
      `fullcontact` varchar(35) DEFAULT NULL,
      `regserver` varchar(20) DEFAULT NULL,
      `useragent` varchar(20) DEFAULT NULL,
      `lastms` int(11) DEFAULT NULL,
      
      
     
      
      `md5secret` varchar(40) DEFAULT NULL,
      `remotesecret` varchar(40) DEFAULT NULL,
      
      
      
     
      
     
      `promiscredir` enum('yes','no') DEFAULT NULL,
      `useclientcode` enum('yes','no') DEFAULT NULL,
     
      
     
      `callcounter` enum('yes','no') DEFAULT NULL,
      `allowoverlap` enum('yes','no') DEFAULT NULL,
      
      `maxcallbitrate` int(11) DEFAULT NULL,
      `rfc2833compensate` enum('yes','no') DEFAULT NULL,
      
      
      `t38pt_usertpsource` varchar(40) DEFAULT NULL,
      `regexten` varchar(40) DEFAULT NULL,
      `fromdomain` varchar(40) DEFAULT NULL,
      `fromuser` varchar(40) DEFAULT NULL,
      
      `defaultip` varchar(40) DEFAULT NULL,
      `rtptimeout` int(11) DEFAULT NULL,
      `rtpholdtimeout` int(11) DEFAULT NULL,
      `sendrpid` enum('yes','no') DEFAULT NULL,
      `outboundproxy` varchar(40) DEFAULT NULL,
      `callbackextension` varchar(40) DEFAULT NULL,
      `registertrying` enum('yes','no') DEFAULT NULL,
      `timert1` int(11) DEFAULT NULL,
      `timerb` int(11) DEFAULT NULL,
      `qualifyfreq` int(11) DEFAULT NULL,
      `constantssrc` enum('yes','no') DEFAULT NULL,
      `contactpermit` varchar(40) DEFAULT NULL,
      `contactdeny` varchar(40) DEFAULT NULL,
      `usereqphone` enum('yes','no') DEFAULT NULL,
      `textsupport` enum('yes','no') DEFAULT NULL,
      `faxdetect` enum('yes','no') DEFAULT NULL,
      `buggymwi` enum('yes','no') DEFAULT NULL,
      `auth` varchar(40) DEFAULT NULL,
      `fullname` varchar(40) DEFAULT NULL,
      `trunkname` varchar(40) DEFAULT NULL,
      `cid_number` varchar(40) DEFAULT NULL,
      `callingpres` enum('allowed_not_screened','allowed_passed_screen','allowed_failed_screen','allowed','prohib_not_screened','prohib_passed_screen','prohib_failed_screen','prohib') DEFAULT NULL,

      `parkinglot` varchar(40) DEFAULT NULL,
      `hasvoicemail` enum('yes','no') DEFAULT NULL,
      `subscribemwi` enum('yes','no') DEFAULT NULL,
      `vmexten` varchar(40) DEFAULT NULL,
      `autoframing` enum('yes','no') DEFAULT NULL,
      `rtpkeepalive` int(11) DEFAULT NULL,
      `call-limit` int(11) DEFAULT NULL,
      `g726nonstandard` enum('yes','no') DEFAULT NULL,
      `ignoresdpversion` enum('yes','no') DEFAULT NULL,
      
      
    """
    YES_NO_SELECTION = [
        ('yes', 'yes'),
        ('no', 'no'),
    ]
    
        
    def _get_states(self, cr, uid, ids, field_names, arg, context):
        res = {}
        try:
            client = ari.connect(ARI_URL, ARI_LOGIN, ARI_PASS) 
        except ConnectionError:
            for id in ids:
                msg = 'Connection error'
                res[id] = {'device_state': msg,
                    'endpoint_state': msg
                }
            return res
        
        for sip_peer in self.browse(cr, uid, ids, context=context):
            res[sip_peer.id] = {}
            try:
                res[sip_peer.id]['device_state']  = client.repositories['deviceStates'].api.get(
                    deviceName='SIP%%2F%s' % sip_peer['name']).json()['state']
                    
            except HTTPError, e:
                    res[sip_peer.id]['device_state'] = 'INVALID'
            try:
                res[sip_peer.id]['endpoint_state'] = client.endpoints.get(tech='SIP', 
                    resource=sip_peer['name']).json['state']
            except HTTPError, e:
                    res[sip_peer.id]['endpoint_state'] = 'invalid'
        del client
        return res
        
    
    # Fucking API mix
    """
    def _get_mailbox(self, cr, uid, ids, name, args, context=None):
        res = {}
        Exten = self.pool['astpbx.exten']
        for exten in Exten.browse(cr, uid, ids, context=context):
            print 'EEX', exten
            user = Exten.get_user(cr, uid, [exten.id], context=context)
            if user:
                print 'MAILBOX', user[0].partner_id.email
                res[exten.id] = user[0].partner_id.email
        return res
    """
    
    def _invalidate_states(self, cr, uid, ids, context=None):
        pass

    _columns = {
        'name': fields.char(string='Name', required=True),
        'secret': fields.char(string='Secret', size=40),
        'callerid': fields.char(string='Caller ID', size=40),
        'host': fields.char(string='Host', size=40),
        'type': fields.selection((
                ('friend', 'friend'),
                ('user', 'user'),
                ('peer', 'peer'),
            ), 'Type'),
        'dynamic': fields.selection(YES_NO_SELECTION, 'Dynamic'),
        'context': fields.many2one('astpbx.dialplan', string='Context'),
        # Network options
        # TODO: nat=yes is deprecated, use nat=force_rport,comedia instead
        'nat': fields.selection(YES_NO_SELECTION + [
            ('never', 'never'),
            ('route', 'route')],
            'Nat'),
        'qualify': fields.char(size=40, string='Qualify'),
        'ipaddr': fields.char(size=15, string='IP address'),
        'port': fields.integer(size=5, string='Port'),
        'transport': fields.selection((
                ('udp', 'udp'),
                ('tcp','tcp'),
                ('udp,tcp', 'udp,tcp'),
                ('tcp,udp', 'tcp,udp')),
                'Transport'),
        #'permit': 
        #'deny':
        # SIP
        'insecure': fields.char(size=40, string='Insecure'),
        # VM options
        #'mailbox': fields.function(_get_mailbox, type='char', string='Mailbox'),
        'mailbox': fields.char(string='Mailbox'),
        # Telephony
        'busylevel': fields.integer(string='Busy level'),
        'allowsubscribe': fields.char(string='Allow subscribe', selection=YES_NO_SELECTION),
        
        # Dynamic functions
        'device_state': fields.function(_get_states, string='Device state', 
            type='char', method=True, multi='states', select=1,), 
        'endpoint_state': fields.function(_get_states, string='Endpoint state', 
            type='char', method=True, multi='states', select=1),
    }

    
    def _gen_random_secret(self, cr, uid, context=None):
        import random, string
        secret = [s for s in string.printable[:-16] if s!='='] # remove formaters and equal
        random.shuffle(secret)
        return ''.join(secret[:10])
                    
    
    _defaults = {
        'host': 'dynamic',
        'nat': 'yes',
        'type': 'friend',
        'secret': _gen_random_secret,
        'busylevel': '1',
        'allowsubscribe': 'yes',
    }
    
    
sip_peer()
