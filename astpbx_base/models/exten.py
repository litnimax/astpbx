from openerp import api
from openerp.osv import orm, fields


"""
Call rule destinations:
- Play busy
- Congestion
- Hangup
- Menu
- Exten
- Internal
- Call group
- Provider
"""


class exten(orm.Model):
    _name = 'astpbx.exten'
    _order = 'name'
    
    exten_builders = [
        (1, 'astpbx.exten', 'noop_builder'),
        (100, 'astpbx.exten', 'peer_builder'),
    ]
    
    
    def _get_exten_types(self, cr, uid, context=None):
        # Override me to add exten types
        return [
            ('peer', 'Peer'),
            ('provider', 'Provider'),
            ('ringall', 'Ring group'),
            ('user', 'User'),            
            #('dial', 'Dial pattern'),
            ('custom', 'Custom dialplan'),
        ]
    

    def on_change_type(self, cr, uid, ids, exten_type, context=None):
        if exten_type == 'custom':
            return {'value': {
                'allow_subscribe': False,
                'is_recorded': False
                }
            } 


    def _get_name(self, cr, uid, ids, fields, arg, context=None):
        x = {}
        for record in self.browse(cr, uid, ids, context=context):
            if record.exten_type == 'peer':
                User = self.pool.get('res.users')
                users = User.search(cr, uid, [('exten', '=', '%s' % record.pattern)], context=context)
                if users:
                    user = User.browse(cr, uid, [users[0]], context)
                    x[record.id] = '%s@%s [%s]' % (record.pattern, record.peer_id.name, user.partner_id.name)
                else:
                    x[record.id] = '%s@%s' % (record.pattern, record.peer_id.name)
            elif record.exten_type == 'custom':
                x[record.id] = '%s@%s' % (record.pattern, record.custom_name)
        return x
            
                
    def _get_partner_user_exten(self, cr, uid, ids, context=None):
        # Executed on res.partner, used to reset exten name when partner name is changed
        users = self.browse(cr, uid, ids, context=context)[0].user_ids
        if not users or not users[0].exten:
            return []
        else:
            return self.pool['astpbx.exten'].search(cr, uid, [('pattern', '=', users[0].exten)], context=context)
    
    
    def _get_user_exten(self, cr, uid, ids, context=None):
        # Executed on res.users, used to reset exten name when user name is changed
        user = self.browse(cr, uid, ids, context=context)[0]
        if not user or not user.exten:
            return []
        else:
            with_exten_ids = self.pool['astpbx.exten'].search(cr, uid, [('pattern', '=', user.exten)], context=context)
            with_name_ids = self.pool['astpbx.exten'].search(cr, uid, [('name', 'like', '%%%s%%' % user.partner_id.name)], context=context)
            res = with_exten_ids + with_name_ids
            return res
    
    
    _columns = {
        'sequence': fields.integer(required=True),
        'name': fields.function(_get_name, method=True, type='char', string='Name', store={
            'astpbx.exten': (
                lambda self, cr, uid, ids, c={}: ids,
                ['exten_type', 'peer_id', 'pattern', 'custom_dialplan'],
                1,
                ),
                'res.partner': (_get_partner_user_exten, ['name'], 1),
                'res.users': (_get_user_exten, ['exten'], 1),
            }
        ),
        'pattern': fields.char(string='Exten mask'),
        'exten_type': fields.selection(_get_exten_types, 'Exten type', required=True),
        'peer_id': fields.many2one('astpbx.sip.peer', string='Peer'),
        #'peer_name': fields.related('peer_id', 'name', type='char', string='Peer name'),
        'dialplan': fields.text(string='Custom dialplan', 
            help='Enter extension priorities here. E.g.:\nn,Goto(default,${EXTEN},1)\nn,MeetMe(room)'),
        'custom_name': fields.char(string='Custom exten name'),
        'allow_subscribe': fields.boolean(string='Allow subscribe'),
    }
    
    _sql_constraints = [
        ('exten_uniq', 'unique(pattern)', 'The exten pattern must be unique'),
    ]
    

    def _validate_peer(self, cr, uid, ids, context=None):
        """ Make peer necessary on peer types """
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.exten_type == 'peer' and not rec.peer_id:
                return False
            return True

    
    _constraints = [
        (_validate_peer, 'You must select a peer from list of peers or create one right now!', ['peer_id']),
    ]
    
    
    _defaults = {
        'allow_subscribe': True,
    }

    
    @api.one
    @api.model
    def get_user(self):
        user = self.env['res.users'].search([('exten','=',self.pattern)])
        return user

    @api.one
    def same(self, cmd):
        return ' same =>  %sn,%s' % (' ' * len(self.pattern), cmd)

    @api.one
    @api.model
    def noop_builder(self):
        return 'exten => %s,1,NoOp\n' % self.pattern

    @api.one
    @api.model
    def peer_builder(self):
        res = ''
        user = self.get_user()
        if user:
            user = user[0]
            call_rules = self.env['astpbx.user.callrule'].search([('user_id','=',user.id)])
            if call_rules:
                for rule in call_rules:
                    res += self.same('Dial(SIP/%s)\n' % rule.number)[0]
            else:
                res += self.same('Dial(SIP/%s/${EXTEN})\n' % self.peer_id.name)[0] 
            # VM
            if self.peer_id.mailbox:
                res+= self.same('VoiceMail(u${EXTEN})')[0]
        return res
        
    @api.one
    def gen_dialplan(self):
        res = ''
        cr, uid, context = self.env.args
        for (prio, model, builder) in sorted(self.exten_builders):
                M = self.pool[model]
                method = getattr(M, builder)
                res += method(cr, uid, [self.id], context={})[0]
        return res
        
