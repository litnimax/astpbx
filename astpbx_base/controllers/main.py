from openerp import http
from openerp.http import request
from openerp import SUPERUSER_ID

# TODO:
EXTENSIONS_CONF = '/home/max/asterisk/a12/etc/asterisk/extensions.conf'
ASTERISK_CMD = '/home/max/asterisk/a12/sbin/asterisk -rx "%s"'

class astpbx_api(http.Controller):
    
    builders = []

    @http.route('/astpbx/get_callerid_name', auth='none', type='http')
    def get_callerid_name(self, phone):
    
        # Empty callerid name
        if not phone:
            return ''
    
        Partner = request.registry.get('res.partner')
        # TODO: Order by user's contact so that when contacts are duplicated each user gets his own
        partner = Partner.search_read(request.cr, SUPERUSER_ID, [('phone','=','%s' % phone)], ['name'], context=request.context)
        if partner:
            # Return the 1-st name found
            return partner[0]['name']
        else:
            return ''
            
            
    @http.route('/astpbx/extensions.conf', auth='none', type='http')
    def generate_conf(self):
        # Test :-)
        for builder in self.builders:
            print builder()
            
        AstContext = request.registry.get('astpbx.dialplan')
        Exten = request.registry.get('astpbx.exten')
        Template = request.registry.get('astpbx.conf.template')
        conf = []
        
        # Get templates
        for name in ['extensions.globals', 'extensions.macro-stdexten']:
            t = Template.search_read(request.cr, SUPERUSER_ID, domain=[
                                    ('name', '=', name)])
            conf.append('[%s]' % t[0]['section'])
            conf.append(t[0]['options'])
            conf.append(';')
        
        # 
        ast_contexts = AstContext.search_read(request.cr, SUPERUSER_ID, context=request.context)
        for c in ast_contexts:
            conf.append(u'[%s]; %s' % (c['id'], c['name']))
            
            # Process extensions first
            exten_ids = Exten.search(request.cr, SUPERUSER_ID, 
                            [
                                ('id', 'in', c['exten_ids']),
                                ('exten_type','=','peer')
                            ], 
                            order='pattern ASC',
                            context=request.context)
            for exten in Exten.browse(request.cr, SUPERUSER_ID, exten_ids, context=request.context):
                # Add hint 1-st
                if exten['allow_subscribe']:
                    conf.append('exten => %s,hint,SIP/%s' % (exten['pattern'], exten['peer_id'].name))
                
                conf.append('exten => %s,1,NoOp' % exten['pattern'])
                # For nice formatting offset
                exten_gap = ' ' * len(' %s,' % exten['pattern'])
                                
                # Check recording
                if exten['is_recorded'] or c['is_recorded']:
                    conf.append(' same =>%sn,MixMonitor(${UNIQUEID}.wav)' % exten_gap)
                conf.append(' same =>%sn,Macro(stdexten,${EXTEN},SIP/%s)' % (
                    exten_gap,
                    exten['peer_id'].name))
            
            # Process dial peers next
            """
            for exten in Exten.search_read(request.cr, SUPERUSER_ID, 
                            domain=[
                                ('id', 'in', c['exten_ids']),
                                ('exten_type','=','dial')
                            ], 
                            order='dial_pattern ASC',
                            context=request.context):
                ast_line = 'exten => %s,1,Dial(${EXTEN}@SIP/%s,60,T)' % (
                    exten['dial_pattern'],
                    exten['dial_peer_name'],
                )
                conf.append(ast_line)
            """
            # Process custom
            exten_ids = Exten.search(request.cr, SUPERUSER_ID, 
                            [
                                ('id', 'in', c['exten_ids']),
                                ('exten_type','=','custom')
                            ], 
                            order='pattern ASC',
                            context=request.context)
            for exten in Exten.browse(request.cr, SUPERUSER_ID, exten_ids, context=request.context):
                exten_gap = ' ' * len(' %s,' % exten['pattern'])
                conf.append('exten => %s,1,NoOp' % exten['pattern'])
                for line in exten['dialplan'].split():
                    conf.append(' same =>%s%s' % (exten_gap, line))
            
        data = u'\n'.join(conf)
        open(EXTENSIONS_CONF, 'w').write(data)
        import commands
        return '%s' % commands.getoutput(ASTERISK_CMD % 'dialplan reload') 
            
        

